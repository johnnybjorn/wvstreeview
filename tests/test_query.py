import numpy as np
import pandas as pd
from unittest import TestCase
from wvs import query
from wvs.util import WVSException


class TestQuery(TestCase):

    def test_country_code_of(self):
        codebook = {'V2': {'coding': {100: 'Kings Landing', 200: 'Dorne'}}}

        # It should be case-insensitive.
        self.assertEqual(query.country_code_of(codebook, 'DORNE'), 200)
        with self.assertRaises(LookupError):
            query.country_code_of(codebook, 'Winterfell')

    def test_country_subdf(self):
        df = pd.DataFrame([{'V2': 100, 'X': 10},
                           {'V2': 100, 'X': 15},
                           {'V2': 200, 'X': 1000}])

        self.assertEqual(query.country_subdf(df, 100)['X'].sum(), 25)

    def test_drop_missing_cols(self):
        df = pd.DataFrame([{'V2': 100, 'X': 10, 'Y': np.NaN, 'Z': np.NaN},
                           {'V2': 100, 'X': 15, 'Y': np.NaN, 'Z': 1},
                           {'V2': 200, 'X': 1000, 'Y': np.NaN, 'Z': 2}])

        # It only drops *completely* missing columns.
        self.assertEqual(set(query.drop_missing_cols(df).columns),
                         set(['V2', 'X', 'Z']))

    def test_without_dep_var(self):
        self.assertEqual(query.without_dep_var(['A', 'B', 'C'], 'B'),
                         ['A', 'C'])

    def test_available_vars(self):
        df = pd.DataFrame([{'V2': 100, 'X': 10, 'Y': np.NaN, 'Z': np.NaN},
                           {'V2': 100, 'X': 15, 'Y': np.NaN, 'Z': 1},
                           {'V2': 200, 'X': 1000, 'Y': np.NaN, 'Z': 2}])

        self.assertEqual(set(query.available_vars(df, ['X', 'Y', 'Q'])),
                         set(['X', 'Y']))

    def test_search_codebook_descs(self):
        codebook = {'a': {'desc': "a measure of trust"},
                    'b': {'desc': "trust rescaled"},
                    'c': {'desc': "income"}}

        sub_codebook = query.search_codebook_descs(codebook, "TRUST")
        self.assertEqual(set(sub_codebook.keys()), set(['a', 'b']))

    def test_extract_prob_na(self):
        codebook = {'a': {'coding': {-1: 'NA', 10: 'OK'}},
                    'b': {'coding': {-2: 'NAN', -3: 'na'}},
                    'c': {'coding': {10: 'YES', 20: 'NO'}}}

        result = query.extract_prob_na(codebook)

        self.assertEqual(set(result.keys()), set(['NA', 'NAN']))
        self.assertEqual(result['NA'], set(['a', 'b']))
        self.assertEqual(result['NAN'], set(['b']))

    def test_select_country_names(self):
        names = ["Beyond the Wall", "Dorne", "The Crownlands",
                 "The Iron Islands", "The North", "The Reach",
                 "The Riverlands", "The Stormlands", "The Vale of Arryn",
                 "The Westerlands"]

        coding = {'V2': {'coding': dict(zip(range(0, 10), names))}}

        # Without prioritization.
        self.assertEqual(query.select_countries_names(coding), names)

        # With prioritization.
        priorities = ["The North", "DORNE", "Narnia"]  # Case-insensitive.
        expected = ["The North", "Dorne", "Beyond the Wall",  "The Crownlands",
                    "The Iron Islands",  "The Reach", "The Riverlands",
                    "The Stormlands", "The Vale of Arryn", "The Westerlands"]
        self.assertEqual(query.select_countries_names(coding, priorities),
                         expected)

    def test_describe_query(self):
        q = query.describe_query("WV1", "United States", False)
        self.assertEqual(q['wave'], "WV1")
        self.assertEqual(q['country_name'], "United States")
        self.assertEqual(q['drop_na_columns'], False)
        self.assertFalse(q['ignoring'])
        self.assertFalse(q['including'])

        with self.assertRaises(WVSException):
            query.describe_query(ignoring=set('x'), including=set('y'))

    def test_execute_query(self):
        codebook = {'V2': {'coding': {1: 'United States'}},
                    'X': {'desc': 'An X'},
                    'Y': {'desc': 'An Y'},
                    'Z': {'desc': 'An Z'}}
        data = pd.DataFrame([{'V2': 1, 'X': 10, 'Y': 20, 'Z': np.NaN},
                             {'V2': 1, 'X': 20, 'Y': 30, 'Z': np.NaN},
                             {'V2': 2, 'X': 30, 'Y': 40, 'Z': np.NaN}])

        q = query.describe_query()
        q['ignoring'] = set(['x'])
        q['including'] = set(['x'])
        with self.assertRaises(WVSException):
            query.execute_query(q, codebook, data)

        # With bad country name.
        with self.assertRaises(WVSException):
            query.execute_query(query.describe_query(country_name='Narnia'),
                                codebook, data)

        # With dropping.
        res = query.execute_query(query.describe_query(drop_na_columns=True),
                                  codebook, data)
        self.assertEqual(set(res[1].columns), set(['V2', 'X', 'Y']))

        # Without dropping
        res = query.execute_query(query.describe_query(drop_na_columns=False),
                                  codebook, data)
        self.assertEqual(set(res[1].columns), set(['V2', 'X', 'Y', 'Z']))

        # With limited including
        res = query.execute_query(query.describe_query(including=set('X')),
                                  codebook, data)
        self.assertEqual(set(res[1].columns), set(['X']))

        # With non-existent include
        with self.assertRaises(WVSException):
            query.execute_query(query.describe_query(including=set('BAD')),
                                codebook, data)

        # With ignoring (and default drop_na_columns)
        res = query.execute_query(query.describe_query(ignoring=set('X')),
                                  codebook, data)
        self.assertEqual(set(res[1].columns), set(['V2', 'Y']))

        cb, df = query.execute_query(query.describe_query(), codebook, data)
        self.assertEqual(len(df), 2)
        self.assertEqual(set(df.V2), set([1]))
        self.assertEqual(set(cb.keys()), set(['V2', 'X', 'Y']))


