from unittest import TestCase


import os
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from wvs import univ_tree


FIXTURES_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            "fixtures")


class TestUnivTree(TestCase):
    def setUp(self):
        self.df = pd.DataFrame([[1, 0, 1],
                                [1, 0, 1],
                                [1, 1, 1],
                                [1, 1, 1],
                                [0, 1, 0],
                                [0, 0, 1]],
                               columns=['A', 'B', 'K'])

    ###########################################################################
    # WARNING
    #
    #  The version of `scikit-learn` declared in `environment.yml` fills in
    #  the `value` array for all nodes. But, the same version fill in my other
    #  virtual environment only fills in leaf nodes. Again, it's the same
    #  version according to `pip` and `conda`. There is a bug somewhere, but
    #  I'm not sure where. Sine the scikit-learn versions are the same, I
    #  assume it's in a dependency.
    #
    ###########################################################################
    def test_transcribe(self):
        clf = DecisionTreeClassifier(criterion='entropy')
        clf = clf.fit(self.df[['A', 'B']], self.df['K'])
        result = univ_tree.transcribe_classifier(clf)

        # Column 'A' offers the best information gain. It's the root node.
        self.assertEqual(result['name'], 0)
        self.assertAlmostEqual(result['impurity'], 0.65, delta=0.01)
        self.assertEqual(result['threshold'], 0.5)
        self.assertEqual(result['n'], len(self.df))
        self.assertEqual(len(result['children']), 2)
        self.assertEqual(result['values'], [1.0, 5.0])

        # First left (true) child.
        left = result['children'][0]
        self.assertEqual(left['name'], 1)
        self.assertEqual(left['impurity'], 1.0)
        self.assertEqual(left['threshold'], 0.5)
        self.assertEqual(left['n'], 2)
        self.assertEqual(len(left['children']), 2)
        self.assertEqual(list(clf.tree_.value[1][0]), [1.0, 1.0])
        self.assertEqual(left['values'], [1.0, 1.0])

        # Second right (false) child, a leaf.
        right = result['children'][1]
        self.assertEqual(right['name'], 4)
        self.assertEqual(right['impurity'], 0.0)
        self.assertFalse('threshold' in right)
        self.assertEqual(right['n'], 4)
        self.assertFalse('children' in right)
        self.assertEqual(right['values'], [0.0, 4.0])

        # Left -> Left child.
        left_left = result['children'][0]['children'][0]
        self.assertEqual(left_left['name'], 2)
        self.assertEqual(left_left['impurity'], 0.0)
        self.assertFalse('threshold' in left_left)
        self.assertEqual(left_left['n'], 1)
        self.assertFalse('children' in left_left)
        self.assertEqual(left_left['values'], [0.0, 1.0])

        # Right -> Right child.
        right_right = result['children'][0]['children'][1]
        self.assertEqual(right_right['name'], 3)
        self.assertEqual(right_right['impurity'], 0.0)
        self.assertFalse('threshold' in right_right)
        self.assertEqual(right_right['n'], 1)
        self.assertFalse('children' in right_right)
        self.assertEqual(right_right['values'], [1.0, 0.0])
