from unittest import TestCase

import json
import os
import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from wvs import tree_gen, univ_tree


FIXTURES_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            "fixtures")


class TestTreeGen(TestCase):
    def setUp(self):
        self.df = pd.DataFrame([[1, 0, 1],
                                [1, 0, 1],
                                [1, 1, 1],
                                [1, 1, 1],
                                [0, 1, 0],
                                [0, 0, 1]],
                               columns=['A', 'B', 'K'])

    def test_graphviz_tree_str(self):
        clf = DecisionTreeClassifier(criterion='entropy')
        clf = clf.fit(self.df[['A', 'B']], self.df['K'])
        dot_str = tree_gen.graphviz_tree_str(clf, ['A', 'B'])

        with open(os.path.join(FIXTURES_DIR, "gold.dot")) as f:
            expected = f.read()

        self.assertEqual(dot_str, expected)

    def test_render_as_svg(self):
        with open(os.path.join(FIXTURES_DIR, "gold.dot")) as f:
            dot_str = f.read()

        dot_path = os.path.join(FIXTURES_DIR, "test.dot")
        svg = tree_gen.render_dot_as_svg(dot_str, dot_path)

        # I expect a variety of possible SVG translations. DOT may vary, and I
        # don't want to get into dependency-hell -- or, use Docker. This test
        # is weak, but should catch glaring problems immediately.
        self.assertTrue("B &lt;= 0.5" in svg)

    def test_annotate_univ_tree(self):
        # If this first chunk of code fails, check `test_univ_tree.py`.
        clf = DecisionTreeClassifier(criterion='entropy')
        clf = clf.fit(self.df[['A', 'B']], self.df['K'])
        tree = univ_tree.transcribe_classifier(clf)
        codebook = {'A': {'coding': 'CODING A', 'desc': 'A variable'},
                    'B': {'coding': 'CODING B', 'desc': 'B variable'},
                    'K': {'coding': 'CODING K', 'desc': 'K variable'}}

        res = tree_gen.annotate_univ_tree(tree,
                                          clf.tree_,
                                          'K',
                                          ['A', 'B'],
                                          codebook)

        with open(os.path.join(FIXTURES_DIR, "gold_tree.json")) as f:
            expected = f.read()

        self.assertEqual(tree, res)  # Mutating
        self.assertEqual(json.dumps(tree, indent=4), expected)  # JSON-ready

    def test_infer_tree(self):
        clf, labels = tree_gen.infer_tree(self.df, 'K')
        self.assertEqual(labels, ['A', 'B'])

    def test_first_val_over(self):
        df = self.df.copy()
        df['K'] = np.where(df['K'] == 0, 100, 200)
        clf = tree_gen.infer_tree(df, 'K')[0]
        self.assertEqual(tree_gen.class_value_at_idx(clf), 100)

    def test_extract_scores(self):
        clf = DecisionTreeClassifier(criterion='entropy')
        clf = clf.fit(self.df[['A', 'B']], self.df['K'])

        scores = tree_gen.extract_scores(clf, self.df, ['A', 'B'])
        self.assertEqual(scores, [1.0, 1.0, 1.0, 1.0, 0.0, 1.0])

    def test_extract_roc_auc_score(self):
        clf = DecisionTreeClassifier(criterion='entropy')
        clf = clf.fit(self.df[['A', 'B']], self.df['K'])

        score = tree_gen.extract_roc_auc_score(clf, self.df, ['A', 'B'], 'K')
        self.assertEqual(score, 1.0)

        clf.classes_ = [1, 2, 3]
        with self.assertRaises(AssertionError):
            tree_gen.extract_roc_auc_score(clf, self.df, ['A', 'B'], 'K')
