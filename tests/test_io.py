from __future__ import unicode_literals
from __future__ import absolute_import

from unittest import TestCase

import os
import pandas as pd

from wvs import io


FIXTURES_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            "fixtures")


class TestIO(TestCase):

    def test_header_from_coding(self):
        example = {'indices': {0: "A", 1: "B", 2: "C"}}
        expected = ["A", "B", "C"]
        self.assertEqual(io._header_from_coding(example), expected)

    def test_load_coding(self):
        gold_path = os.path.join(FIXTURES_DIR, "gold.json")
        coding = io.load_coding(gold_path)

        self.assertEqual(coding['indices'], {0: "V1", 1: "V2"})
        self.assertEqual(coding['codebook']['V1']['coding'],
                         {-4: "Not asked", 1: "1981-1984"})

    def test_load_wave(self):
        codebook, df = io.load_wave(FIXTURES_DIR)

        self.assertEqual(len(codebook), 2)
        self.assertEqual(len(df.columns), 2)
        self.assertEqual(len(df), 3)

