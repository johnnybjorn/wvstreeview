from unittest import TestCase

import os
from wvs import util
import pandas as pd
import json

FIXTURES_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            "fixtures")

class TestUtil(TestCase):

    def test_to_num(self):
        for s, expected in [("1", 1), ("1.0", 1.0), ("-1.5", -1.5)]:
            self.assertEqual(util.to_num(s), expected)

        with self.assertRaises(ValueError):
            util.to_num("")

    def test_keys_to_numeric(self):
        example = {"1": "A", "1.5": "B"}
        expected = {1: "A", 1.5: "B"}
        self.assertEqual(util.keys_to_numeric(example), expected)

    def test_set_negative_values_to_nan(self):
        df = pd.DataFrame({'x': [-3, 0], 'y': [0, -4]})
        util.set_negative_values_to_nan(df)

        df = df.isnull()
        self.assertTrue(df['x'][0])
        self.assertTrue(df['y'][1])
        self.assertFalse(df['x'][1])
        self.assertFalse(df['y'][0])

    def test_parse_literate_json(self):
        literate_path = os.path.join(FIXTURES_DIR, "gold.json.literate")

        with open(literate_path + '.parsed') as f:
            expected = json.load(f)

        with open(literate_path) as f:
            parsed = util.parse_literate_json(f.read())

        self.assertEqual(parsed, expected)

    def test_wvs_exception(self):
        was_raised = False
        try:
            raise util.WVSException("My basic exception")
        except util.WVSException as e:
            self.assertEqual(str(e), "WVSException: My basic exception")
            was_raised = True
        self.assertTrue(was_raised)
