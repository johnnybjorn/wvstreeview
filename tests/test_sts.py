from __future__ import unicode_literals

from unittest import TestCase

import io
import os
from wvs import sts


FIXTURES_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            "fixtures")


class TestSTS(TestCase):

    def test_partition_decl_and_def_lines(self):
        gold_path = os.path.join(FIXTURES_DIR, "gold.sts")
        with io.open(gold_path, encoding='utf8') as f:
            lines = f.readlines()
            decls, defs = sts._partition_decl_and_def_lines(lines)
            self.assertEqual(len(decls), 2)
            self.assertEqual(len(defs), 201)

    def test_parse_decl_line(self):
        cases = [
            ("   V2\t(F3)\t[<= -1]\t{Country/region}\t\V2  ",
             ("V2", "V2", "Country/region")),
            ("   V2\t(F3)\t{Country/region}\t\V2  ",
             ("V2", "V2", "Country/region")),
            ("   V2\t(F3)\t{Country/region}\t\V  ",
             ("V2", "V", "Country/region")),
        ]

        for line, expected in cases:
            self.assertEqual(sts._parse_decl_line(line), expected)

    def test_n_columns(self):
        path = os.path.join(FIXTURES_DIR, "five_columns.tsv")
        self.assertEqual(sts.n_columns(path, '\t'), 5)

    def test_parse_sts(self):
        gold_path = os.path.join(FIXTURES_DIR, "gold.sts")
        coding = sts.parse_sts(gold_path)

        self.assertEqual(coding['indices'], {0: 'V1', 1: 'V2'})
        self.assertEqual(coding['codebook']['V1']['coding'][-4], "Not asked")
        self.assertEqual(coding['codebook']['V2']['coding'][144], "Sri Lanka")
