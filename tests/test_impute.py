from __future__ import unicode_literals
from __future__ import absolute_import

import numpy as np
import pandas as pd

from unittest import TestCase
from numpy.testing import assert_array_equal
from wvs import imputation

class TestImpute(TestCase):

    def test_fill_na_with_most_frequent(self):
        df = pd.DataFrame([[np.NaN, 3, 100],
                           [np.NaN, 2, 100],
                           [np.NaN, 3, -50],
                           [1, np.NaN, np.NaN]],
                          columns=['A', 'B', 'C'])

        expected = pd.DataFrame([[1, 3, 100],
                                 [1, 2, 100],
                                 [1, 3, -50],
                                 [1, 3, 100]],
                                columns=['A', 'B', 'C'])

        result = imputation.fill_na_with_most_frequent(df)
        assert_array_equal(result, expected)
        self.assertEqual(list(result.columns), list(expected.columns))

    def test_impute_wave(self):
        df = pd.DataFrame([[np.NaN, 3, 100, 1],
                           [np.NaN, 2, 100, 1],
                           [np.NaN, 3, -50, 1],
                           [1, np.NaN, np.NaN, 1],
                           [5, 4, 3, 2],
                           [5, 4, 3, 2],
                           [np.NaN, np.NaN, np.NaN, 2]],
                          columns=['A', 'B', 'C', 'V2'])

        expected = pd.DataFrame([[1, 3, 100, 1],
                                 [1, 2, 100, 1],
                                 [1, 3, -50, 1],
                                 [1, 3, 100, 1],
                                 [5, 4, 3, 2],
                                 [5, 4, 3, 2],
                                 [5, 4, 3, 2]],
                                columns=['A', 'B', 'C', 'V2'])

        imputer = imputation.fill_na_with_most_frequent
        result = imputation.impute_wave(df, imputer)
        assert_array_equal(result, expected)
        self.assertEqual(list(result.columns), list(expected.columns))
