#!/usr/bin/env python

from __future__ import print_function
from __future__ import unicode_literals

import io
import os
import json
from wvs.sts import parse_sts, n_columns

DATA_DIR = os.path.join(os.curdir, "data", "raw")
WVS_DIRS = ["WV{}_Data_ascii_delimited_v_2015_04_18".format(wave)
            for wave in range(1,7)]

if __name__ == '__main__':
    for sub_dir in WVS_DIRS:
        dir_path = os.path.join(DATA_DIR, sub_dir)
        sts_name = [f for f in os.listdir(dir_path) if f.endswith(".sts")][0]
        file_path = os.path.join(dir_path, sts_name)

        print("Parsing:", file_path)
        parsed_info = parse_sts(file_path)

        # Sanity check.
        per_row = n_columns(file_path.replace(".sts", ".dat"))
        assert(per_row == len(parsed_info["indices"]))

        json_path = os.path.join(dir_path, sts_name.replace(".sts", ".json"))
        with io.open(json_path, 'w', encoding='utf8') as f:
            f.write(json.dumps(parsed_info, f, ensure_ascii=False))
