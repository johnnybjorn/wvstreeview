#!/usr/bin/env python

import json
import os

from wvs.imputation import impute_wave, fill_na_with_most_frequent
from wvs.io import load_wave, survey_paths


BASE_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
DATA_DIR = os.path.join(BASE_DIR, 'data', 'raw')
OUTPUT_DIR = os.path.join(BASE_DIR, 'data', 'clean', 'weakly_imputed')


if __name__ == '__main__':
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)

    for survey, path in survey_paths(DATA_DIR):
        print "Imputing over survey: " + survey

        # Setup the output directory.
        dir_name = os.path.split(path)[-1]
        output_dir = os.path.join(OUTPUT_DIR, dir_name)
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        csv_path = os.path.join(output_dir, dir_name + ".dat")
        cb_path = csv_path.replace('.dat', '.json')

        # Load and impute
        codebook, data = load_wave(path)
        coding = {'codebook': codebook}
        data = impute_wave(data, fill_na_with_most_frequent)
        indices = (str(i) for i in range(len(data.columns)))
        coding['indices'] = dict(zip(indices, data.columns))

        # Save new.
        data.to_csv(csv_path, index=False, header=False)
        with open(cb_path, 'w') as f:
            json.dump(coding, f)
