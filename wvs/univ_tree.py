from sklearn.tree._tree import TREE_LEAF


def _translate_leaf_node(tree, node_id):
    return dict(name=node_id,
                n=tree.n_node_samples[node_id],
                impurity=tree.impurity[node_id],
                values=list(tree.value[node_id][0]))


def _translate_inner_node(clf, tree, node_id, left, right):
    return dict(name=node_id,
                n=tree.n_node_samples[node_id],
                impurity=tree.impurity[node_id],
                importance=clf.feature_importances_[tree.feature[node_id]],
                children=[left, right],
                threshold=tree.threshold[node_id],
                values=list(tree.value[node_id][0]))


def transcribe_classifier(clf, tree=None, node_id=0):
    """Translate the inferred tree to a universal JSON-serializable format."""
    if tree is None:
        tree = clf.tree_

    # Get the descendant node ids.
    left_id = tree.children_left[node_id]
    right_id = tree.children_right[node_id]

    if left_id == TREE_LEAF:
        return _translate_leaf_node(tree, node_id)
    else:
        left = transcribe_classifier(clf, tree, left_id)
        right = transcribe_classifier(clf, tree, right_id)
        return _translate_inner_node(clf, tree, node_id, left, right)
