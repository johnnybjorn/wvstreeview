from __future__ import unicode_literals

import os
import subprocess
from cStringIO import StringIO
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import metrics


def graphviz_tree_str(clf, x_labels):
    """
    Generate a graphviz decision tree portrayal without hitting the disk.

    :param clf: The Decision tree classifier
    :param x_labels: The feature names used for independent variables.
    :return: the string of the GraphViz dot portrayal for the given tree.
    """
    dot_data = StringIO()
    export_graphviz(clf, out_file=dot_data, feature_names=x_labels)
    return dot_data.getvalue()


def render_dot_as_svg(dot_str, dot_path):
    """
    Write a dot script and render the it as an SVG file.

    :param dot_str: The string of the dot-language script.
    :param dot_path: The path to the .svg and .dot files. Both files are
        unlinked after rendering.
    :return: the SVG rendering of `dot_str`
    """
    assert dot_path.endswith('.dot')

    with open(dot_path, 'w') as f:
        f.write(dot_str)

    svg_path = dot_path.replace('.dot', '.svg')
    subprocess.call(['dot', '-Tsvg', dot_path, '-o', svg_path])
    os.unlink(dot_path)

    with open(svg_path, 'r') as f:
        svg = f.read()

    os.unlink(svg_path)

    return svg


def annotate_univ_tree(univ_tree, tree_, dep_var, feature_names, codebook):
    """
    Annotates the universal tree with proper names, descriptions, and codes.

    :param univ_tree: The d3-ready, universal json-tree data structure
    :param tree_: The tree (i.e `clf.tree_`) inferred by scikit-learn
    :param dep_var: The name of the dependent variable (i.e. example label)
    :param feature_names: The names of the features (ind. variables)
    :param codebook: The full codebook for the underlying data

    :return: The annotated tree. Note, this is not a copy of the original tree.
    """
    if 'children' in univ_tree:
        node_id = univ_tree['name']
        name = feature_names[tree_.feature[node_id]]
        cb = codebook[name]

        for child in univ_tree['children']:
            annotate_univ_tree(child, tree_, dep_var, feature_names, codebook)
    else:
        name = dep_var
        cb = codebook[name]

    univ_tree['name'] = name
    univ_tree['desc'] = cb['desc']
    univ_tree['codes'] = cb['coding'] if 'coding' in cb else {}

    return univ_tree


def class_value_at_idx(clf, idx=0):
    """
    Return the value of the class (dependent variable) at a given index.

    This code is mostly a reminder and easy-to-remember handle to behavior
    already implemented in sklearn. It came up a few times in iterative
    notebook development. "Wait, what does index i refer to? Oh yea,
    coding 100 -> YES."
    """
    return clf.classes_[idx]


def infer_tree(df, dep_var, max_depth=3, **tree_kwargs):
    """
    Infer the decision tree from data.

    :param df: The training DataFrame.
    :param dep_var: The dependent variable.
    :param max_depth: The maximum depth for the inferred tree.
    :param tree_kwargs: Any additional keyword arguments to sklearn's
        DecisionTreeClassifier.

    :return: A pair of the inferred classifier and the labels for the
        independent variables (features).
    """
    clf = DecisionTreeClassifier(max_depth=max_depth, **tree_kwargs)
    x_labels = [col for col in df.columns if col != dep_var]

    return clf.fit(df[x_labels], df[dep_var]), x_labels


def extract_scores(clf, df, x_labels):
    """
    Use leaf frequencies to assign probability of True.
    """
    dep_df = df[x_labels]
    data = dep_df.values.astype('float32').reshape(dep_df.shape)
    nodes = clf.tree_.apply(data).astype('float32')
    freqs = [clf.tree_.value[int(i)][0] for i in nodes]

    return [f[1] / sum(f) for f in freqs]


def extract_roc_auc_score(clf, df, x_labels, dep_var):
    """
    Compute the ROC area under the curve over a learned decision tree.
    """
    assert len(clf.classes_) == 2, "Only works for binary classifiers."

    scores = extract_scores(clf, df, x_labels)
    labels = df[dep_var] == clf.classes_[1]

    return metrics.roc_auc_score(labels, scores)
