import pandas as pd
from sklearn.preprocessing import Imputer
from wvs.query import country_subdf, drop_missing_cols


def fill_na_with_most_frequent(df):
    """
    Replace NaN values with the respective column's most frequent value.

    WARNING: THIS IS A REALLY BAD IMPUTATION METHOD! IT INTRODUCES
        TREMENDOUS BIAS!

    :return: a new DataFrame with filled in NaN values.
    """
    imputer = Imputer(strategy='most_frequent')
    learn_mtx = imputer.fit_transform(df)

    assert len(df.columns) == learn_mtx.shape[1], \
        "Completely missing column detected"

    return pd.DataFrame(learn_mtx, columns=df.columns)


def impute_wave(df, imputer):
    """
    Impute missing values over an entire wave.

    This entails:

    - Partitioning the data by country.
    - Dropping fully missing columns on any country sub-DataFrame.
    - Imputing over the country sub-DataFrame.
    - Concatenating all the country-specific DataFrames.

    :param imputer: a function that imputes over a DataFrame
    """
    country_dfs = []

    for country_code in set(df['V2']):
        sub_df = country_subdf(df, country_code)
        sub_df = imputer(drop_missing_cols(sub_df))
        country_dfs.append(sub_df)

    return pd.concat(country_dfs).reset_index(drop=True)

