import json
import re
import numpy as np


def to_num(s):
    """Attempts to convert a string the appropriate numeric type."""
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            raise ValueError("Could not convert to num: '{}'".format(s))


def keys_to_numeric(d):
    """
    Convert all keys in a dict to a numeric.

    JSON does not allow for dictionaries with numeric keys. This is
    problematic, because most keys in the coding are numeric.
    """
    return {to_num(k): v for k, v in d.items()}


def set_negative_values_to_nan(df):
    # Warning: x < 0 may mean things like "not applicable". This could result
    # in nonsensical imputations.
    df[df < 0] = np.NaN


LINE_COMMENT_RE = re.compile("^.*?(//.*)$", re.MULTILINE)


def parse_literate_json(s):
    """
    Parse the given string as JSON, extracting Javascript-style `//` comments.

    NOTE: This is a very terrible parser. It works for what I am doing, since
    there are no string embedded `//` comments. But, it is far from robust.
    Instead, its an optimization over the cost of my time and the benefits to
    commented JSON.
    """
    for m in LINE_COMMENT_RE.finditer(s):
        s = s.replace(m.group(1), '')
    return json.loads(s)


class WVSException(Exception):
    """Simple exception wrapper for ease of catching in UI components."""
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "WVSException: {}".format(self.msg)
