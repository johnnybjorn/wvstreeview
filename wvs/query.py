from __future__ import unicode_literals
from collections import defaultdict

import numpy as np

from wvs.util import WVSException


def country_code_of(codebook, name):
    """
    :return: The country code associated with a full name given a codebook.
    """
    name = name.upper()
    d = codebook['V2']['coding']  # 'V2 is harmonized across all waves.

    for k, v in d.items():
        if v.upper() == name:
            return int(k)

    raise LookupError("Country '{}' not found in codebook".format(name))


def country_subdf(data, country_code):
    """
    :return: A sub-DataFrame with limited to a specific country, denoted by the
        given country_code
    """
    return data[data['V2'] == country_code]


def drop_missing_cols(df):
    """
    :return: The given DataFrame less any **completely** columns with
        **completely** missing data.
    """
    return df[df.columns[~df.isnull().all(axis=0)]]


def without_dep_var(var_names, dep_var):
    """
    :return: a copy of `var_names` less `dep_var` if it is present.
    """
    return [x for x in var_names if x != dep_var]


def available_vars(df, target_vars):
    """
    :return: the intersection of the given DataFrame's columns and the target
        variable names.
    """
    return df.columns[np.in1d(df.columns, target_vars)]


def search_codebook_descs(codebook, q):
    """
    :param q: the query string, case-insensitive
    :return: A sub-codebook dictionary for all variables which match the
        query
    """
    q = q.lower()
    matches = {}
    for var_name, decl in codebook.items():
        if q in decl['desc'].lower():
            matches[var_name] = decl.copy()
    return matches


def extract_prob_na(codebook):
    """
    WVS marks bad or missing values with negative numbers.

    :return: A mapping from the description to the set of variables with it,
        for all descriptions that are probably NA.
    """
    nas = defaultdict(set)
    for variable, defn in codebook.items():
        if 'coding' in defn:
            for k, v in defn['coding'].items():
                if k < 0:
                    nas[v.upper()].add(variable)
    return nas


def select_countries_names(codebook, priority_names=[]):
    """
    :return: The names of all the countries present in the given codebook,
        sorted alphabetically. However, names in priority_names appear
        first, in the lists given order.
    """
    country_map = codebook['V2']['coding']

    # Gather all the country names and sort them for human presentation.
    country_names = []
    for k, v in country_map.items():
        if k >= 0:
            country_names.append(v)
    country_names.sort()

    # For any name that matches a prioritized name, move it to the head of
    # the list. This is useful if the researcher is only interested in
    # a handful of countries. It makes point-and-click ops quicker.
    #
    # The name search is case-insensitive.
    name_idx = [n.upper() for n in country_names]
    for name in reversed([name.upper() for name in priority_names]):
        try:
            matched_name = country_names[name_idx.index(name)]

            country_names.remove(matched_name)
            country_names.insert(0, matched_name)

            # Keep two lists in sync with respect to the position.
            name_idx.remove(name)
            name_idx.insert(0, name)
        except ValueError:
            pass

    return country_names


def describe_query(wave='WV6',
                   country_name='United States',
                   drop_na_columns=True,
                   including=None,
                   ignoring=None):
    """
    Generate the query (dictionary) for later execution.

    :param wave: The WVS wave in 'WV#' format.
    :param country_name: The name of the country, as it is declared in the
        codebook.
    :param drop_na_columns: Set to true if fully-missing columns should be
        dropped
    :param including: A set of variables to include in the query result
    :param ignoring: A set of variables to ignore in the query result

    :return: A query as a dictionary.
    """
    if including is None:
        including = set()

    if ignoring is None:
        ignoring = set()

    if including and ignoring:
        raise WVSException("You cannot both include and ignore variables.")

    return {'wave': wave,
            'country_name': country_name,
            'drop_na_columns': drop_na_columns,
            'including': set(including),
            'ignoring': set(ignoring)}


def execute_query(q, codebook, data):
    """
    Execute a query over a set of waves.

    :param q: The query built by `describe_query`, possibly altered.
    :param waves: A dict of wave to (codebook, data).

    :return: A tuple of the codebook and data limited to the query.
    """
    # Find country code.
    try:
        country_code = country_code_of(codebook, q['country_name'])
    except LookupError:
        msg = "Could not find '{country_name}' in Wave {wave}".format(**q)
        raise WVSException(msg)

    # Grab country specific DataFrame.
    sub_df = country_subdf(data, country_code)

    # NA values need to be dropped for many learners.
    if q['drop_na_columns']:
        sub_df = drop_missing_cols(sub_df)

    # Enforce disjoint including and ignoring condition.
    if q['including'] and q['ignoring']:
        raise WVSException("Including or ignoring can be set, not both")

    # Build field list.
    if q['including']:
        fields = []
        valid_cols = set(sub_df.columns)
        for field in q['including']:
            if field not in valid_cols:
                d = {'field': field}
                d.update(q)
                msg = "Field `{field}` not present in {wave}:{country_name}"
                raise WVSException(msg.format(**d))
            else:
                fields.append(field)
    else:
        fields = [field for field in sub_df.columns
                  if field not in q['ignoring']]

    # Return codebook and DataFrame.
    codebook = {k: v for k, v in codebook.items() if k in fields}
    sub_df = sub_df[[col for col in sub_df.columns if col in fields]]

    return codebook, sub_df
