from __future__ import absolute_import
from __future__ import unicode_literals

import io

from wvs.util import to_num

def _partition_decl_and_def_lines(lines):
    """
    Given a list of lines extract the variable definitions and declarations.
    """
    # The STS file starts with a preamble. Verify some expectations about this
    # preamble, but otherwise ignore it.
    format_idx = -1
    for i, line in enumerate(lines):
        if line.startswith("FORMAT"):
            format_idx = i
    assert format_idx != -1
    lines = lines[format_idx:]
    assert lines.pop(0).startswith('FORMAT delimited commas')
    assert lines.pop(0).startswith('VARIABLES')

    # Collect the variable declarations. An empty line followed by a
    # 'VALUES LABEL' line partitions the declarations and definitions.
    decls = []
    while lines:
        line = lines.pop(0)
        if line.strip() == '':
            break
        decls.append(line)
    assert lines.pop(0).startswith('VALUE LABELS')

    # The remaining non-empty lines are definitions.
    defs = [line.strip() for line in lines if line.strip() != '']

    return decls, defs


def _parse_decl_line(decl):
    """
    :return: (var_name, short_name, desc) given a variable declaration line
    """
    # The line is tab-delimited.
    parts = [s.strip() for s in decl.split("\t")]

    # The variable name is the first element, given the trimmed line.
    var_name = parts[0]

    # The description is enclosed by '{' and '}'. But, its index in the parts
    # varies.
    desc = [s for s in parts if s.startswith('{')][0]
    assert desc[-1] == '}'
    desc = desc[1:-1]  # Extract unbracketed.

    # STS seems to have a short-name limitation. The last element declares the
    # variables short name.
    short_name = parts[-1][1:]

    return var_name, short_name, desc


def n_columns(file_path, sep=','):
    """
    :return: the number of columns expected in a character delimited file
    """
    with open(file_path) as f:
        return len(f.readline().split(sep))


def parse_sts(file_path):
    with io.open(file_path, encoding='utf8') as f:
        decls, defs = _partition_decl_and_def_lines(f.readlines())

    # First, parse the declarations and build maps for indices and
    # a variables short name.
    parsed_decls, indices, short_2_long_name = {}, {}, {}
    for i, decl in enumerate(decls):
        var_name, short_name, desc = _parse_decl_line(decl)

        # There should not be duplicates.
        assert var_name not in parsed_decls

        # The order of the declarations should match the order of the columns
        # in the data file.
        indices[i] = var_name

        short_2_long_name[short_name] = var_name

        parsed_decls[var_name] = {'desc': desc, 'coding': {}}

    var_name, code_to_desc = '', None
    for line in defs:
        # New entries start with "\short_name"
        if line.startswith('\\'):

            # Save the last collected definition. But, also save empty
            # dicts, if they exist. I'd rather the KeyError pop up when
            # mapping a coding than when accessing 'codings'.
            if code_to_desc is not None:
                parsed_decls[var_name]['coding'] = code_to_desc
            code_to_desc = {}

            # Grab the short_name and make sure it's been declared.
            short_name = line[1:].strip()
            assert short_name in short_2_long_name, \
                "{} unknown".format(short_name)

            # Set the working variable name.
            var_name = short_2_long_name[short_name]
        else:  # A coding line
            space_idx = line.index(' ')
            value = to_num(line[:space_idx].strip())  # All values are numeric
            desc = line[space_idx+1:].strip()

            # Unquote strings
            if desc[0] == "'" and desc[-1] == "'":
                desc = desc[1:-1]
            if desc[0] == '"' and desc[-1] == '"':
                desc = desc[1:-1]

            # There are some miscoded strings. Ignore them.
            code_to_desc[value] = desc

    # Some entries have no coding. It's also possible the coding hasn't yet
    # been added. This is a safe operation in either case.
    if not parsed_decls[var_name]['coding']:
        parsed_decls[var_name]['coding'] = code_to_desc

    return {'indices': indices, 'codebook': parsed_decls}
