from __future__ import unicode_literals
from __future__ import absolute_import

import json
import os
import io
import pandas as pd
from . util import keys_to_numeric, set_negative_values_to_nan


def _header_from_coding(coding):
    """
    Convert the codings indices mapping into a list suitable for a DFs columns.
    """
    idx = coding['indices']
    return [idx[i] for i in range(len(idx))]


def load_coding(coding_path):
    """
    Load the coding with keys converted to numbers where possible.
    """
    with io.open(coding_path, encoding='utf8') as f:
        coding = json.load(f)

    coding['indices'] = keys_to_numeric(coding['indices'])

    for var_name in coding['codebook'].keys():
        assert 'coding' in coding['codebook'][var_name], \
            "Missing coding in {}: {}".format(var_name, coding_path)
        d = keys_to_numeric(coding['codebook'][var_name]['coding'])
        coding['codebook'][var_name]['coding'] = d

    return coding


def load_wave(survey_dir, assume_negative_is_nan=True):
    """
    Loads the WVS data for a wave serialized to a given directory.

    :param survey_dir: the directory containing the codebook (.json) and
        data (.dat) files
    :param assume_negative_is_nan: if true, this converts all negative numbers
        to NaN
    :return: a tuple of the codebook as a dictionary and data as a dataframe
    """
    data_file = [n for n in os.listdir(survey_dir) if n .endswith('.dat')]
    assert data_file, ".dat file not found"
    data_file = data_file[0]
    data_file_path = os.path.join(survey_dir, data_file)

    coding = load_coding(data_file_path.replace(".dat", ".json"))

    df = pd.read_csv(data_file_path, names=_header_from_coding(coding))

    if assume_negative_is_nan:
        set_negative_values_to_nan(df)

    return coding["codebook"], df


def load_all_waves(data_dir, assume_negative_is_nan=True):  # pragma: no cover
    """
    Load all waves into memory for real-time querying.

    :param assume_negative_is_nan: if true, this converts all negative numbers
        to NaN

    :return: A dictionary mapping each wave to a pair of its codebook and data.
    """
    waves = {}
    for survey, path in survey_paths(data_dir):
        waves[survey] = load_wave(path, assume_negative_is_nan)
    return waves


def survey_paths(data_dir):  # pragma: no cover
    items = []
    for file_name in os.listdir(data_dir):
        path = os.path.join(data_dir, file_name)
        if file_name.startswith("WV") and os.path.isdir(path):
            survey = file_name[0:3]
            items.append((survey, path))
    return items
