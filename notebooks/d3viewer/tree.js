// Generated by CoffeeScript 1.10.0

/*
I based this tree viewer on d3noob's simplification of Mike Bostock's 
collapsable tree example. See: http://bl.ocks.org/d3noob/8326869

I wrote this to meet two different goals.

1) The graphviz tree produced by scikit-learn is ugly. There are measures 
more intuitive than gini. In the case of a dichotomous value, the percent 
with value X is exactly what people want. Moreover, the conditionals were 
confusing. You need to lookup what they mean. 

2) I don't want to impose a build dependency of graphviz. Lots of people in 
CSS don't use package managers in their day-to-day, so it's frustrating to 
them.
 */
var KEY_ORDERING, NO_PARENT, diagonal, explain_node, height, i, kv_pair_table, margin, selectedNode, svg, tree, treeData, update, width,
  hasProp = {}.hasOwnProperty;

NO_PARENT = 'null';

treeData = [];

KEY_ORDERING = ['name', 'desc', 'impurity', 'n', 'values', 'threshold', 'importance'];

kv_pair_table = function(tbl_selection, kv_pairs, header) {
  var elements, rows;
  if (header != null) {
    kv_pairs.unshift(header);
  }
  rows = tbl_selection.selectAll('tr').data(kv_pairs);
  rows.exit().remove();
  rows.enter().append('tr');
  elements = rows.selectAll('td').data(function(d) {
    return d;
  });
  elements.enter().append('td');
  return elements.text(function(d) {
    return d;
  });
};

explain_node = function(node_info) {
  var k, kv_pairs, v;
  kv_pairs = (function() {
    var j, len, results;
    results = [];
    for (j = 0, len = KEY_ORDERING.length; j < len; j++) {
      k = KEY_ORDERING[j];
      if (k in node_info) {
        results.push([k, node_info[k]]);
      }
    }
    return results;
  })();
  kv_pair_table(d3.select('table#node-info'), kv_pairs, ['Key', 'Value']);
  kv_pairs = ((function() {
    var ref, results;
    ref = node_info['codes'];
    results = [];
    for (k in ref) {
      if (!hasProp.call(ref, k)) continue;
      v = ref[k];
      if (+k >= 0) {
        results.push([k, v]);
      }
    }
    return results;
  })()).sort(function(a, b) {
    return parseFloat(a[0]) - parseFloat(b[0]);
  });
  return kv_pair_table(d3.select('table#codes'), kv_pairs, ['Code', 'Description']);
};

margin = {
  top: 40,
  right: 20,
  bottom: 20,
  left: 20
};

width = 800 - margin.right - margin.left;

height = 450 - margin.top - margin.bottom;

i = 0;

tree = d3.layout.tree().size([width, height]);

diagonal = d3.svg.diagonal().projection(function(d) {
  return [d.x, d.y];
});

svg = d3.select('#tree').attr('width', width + margin.right + margin.left).attr('height', height + margin.top + margin.bottom).append('g').attr('transform', "translate(" + margin.left + ", " + margin.top + ")");

selectedNode = void 0;

update = function(root) {
  var link, links, node, nodeElements, nodes;
  nodes = tree.nodes(root).reverse();
  links = tree.links(nodes);
  nodes.forEach(function(d) {
    return d.y = d.depth * 65;
  });
  svg.selectAll('g.node').data([]).exit().remove();
  node = svg.selectAll('g.node').data(nodes, function(d) {
    return d.id || (d.id = (i += 1));
  });
  nodeElements = node.enter().append('g').classed('node', true).attr('transform', function(d) {
    return "translate(" + d.x + ", " + d.y + ")";
  });
  nodeElements.append('circle').attr('r', 10).on('click', function(d) {
    var selectOnly, unselect_only;
    selectOnly = false;
    if (selectedNode != null) {
      unselect_only = selectedNode === this;
      d3.select(selectedNode).classed('selected', false);
      selectedNode = void 0;
    }
    if (unselect_only) {
      return d3.select('#node-info-container').style('visibility', "hidden");
    } else {
      d3.select('#node-info-container').style('visibility', null);
      d3.select(this).classed('selected', true);
      selectedNode = this;
      return explain_node(d);
    }
  });
  nodeElements.append('text').attr('y', function(d) {
    return -20;
  }).attr('dy', '.35em').attr('text-anchor', 'middle').text(function(d) {
    if (d.children) {
      return d.name;
    } else {
      return '';
    }
  }).style('fill-opacity', 1);
  nodeElements.append('text').attr('y', function(d) {
    return 20;
  }).attr('dy', '.35em').attr('text-anchor', 'middle').text(function(d) {
    if (d.children || d._children) {
      return "< " + (d3.round(d.threshold, 3));
    } else {
      return "[" + (d.values.join(' ')) + "]";
    }
  }).style('fill-opacity', 1);
  svg.selectAll('path.link').data([]).exit().remove();
  link = svg.selectAll('path.link').data(links, function(d) {
    return d.target.id;
  });
  return link.enter().insert('path', 'g').attr('class', 'link').attr('d', diagonal);
};

window.updateTreePortrayal = function(jsonString) {
  return update(JSON.parse(jsonString));
};
