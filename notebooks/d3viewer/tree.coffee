###
I based this tree viewer on d3noob's simplification of Mike Bostock's 
collapsable tree example. See: http://bl.ocks.org/d3noob/8326869

I wrote this to meet two different goals.

1) The graphviz tree produced by scikit-learn is ugly. There are measures 
more intuitive than gini. In the case of a dichotomous value, the percent 
with value X is exactly what people want. Moreover, the conditionals were 
confusing. You need to lookup what they mean. 

2) I don't want to impose a build dependency of graphviz. Lots of people in 
CSS don't use package managers in their day-to-day, so it's frustrating to 
them. 
###

# Tree Structure:

NO_PARENT = 'null'

treeData = []

#==============================================================================
# This section takes care of rendering the elaborated details view given a 
# node click.
#==============================================================================

KEY_ORDERING = [
    'name', 'desc', 'impurity', 'n', 'values', 'threshold', 'importance'
]

kv_pair_table = (tbl_selection, kv_pairs, header) ->
    if header?
        kv_pairs.unshift(header)

    rows = tbl_selection.selectAll('tr').data(kv_pairs)
    rows.exit().remove()
    rows.enter().append('tr')
    elements = rows.selectAll('td').data((d) -> d)
    elements.enter().append('td')
    elements.text((d) -> d)

explain_node = (node_info) ->
    kv_pairs = ([k, node_info[k]] for k in KEY_ORDERING when k of node_info)
    kv_pair_table(d3.select('table#node-info'), kv_pairs, ['Key', 'Value'])

    kv_pairs = ([k, v] for own k, v of node_info['codes'] when +k >= 0)
        .sort((a,b) -> parseFloat(a[0]) - parseFloat(b[0]))
    kv_pair_table(d3.select('table#codes'), kv_pairs, ['Code', 'Description'])

#==============================================================================

margin = top: 40, right: 20, bottom: 20, left: 20
width  = 800 - margin.right - margin.left
height = 450 - margin.top - margin.bottom

i = 0

tree = d3.layout.tree().size([width, height])

diagonal = d3.svg.diagonal().projection((d) -> [d.x, d.y]) 

svg = d3.select('#tree')
    .attr('width', width + margin.right + margin.left)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
        .attr('transform', "translate(#{margin.left}, #{margin.top})")


selectedNode = undefined

update = (root) -> 
    nodes = tree.nodes(root).reverse()
    links = tree.links(nodes)

    nodes.forEach((d) -> d.y = d.depth * 65)  # Fixed depth

    # Cheap way to clear everything.
    svg.selectAll('g.node').data([]).exit().remove()

    # Select and bind the 'g.node' elements.
    node = svg.selectAll('g.node')
        .data(nodes, (d) -> d.id or (d.id = (i+=1)))

    # If `g.node` doesn't exist, create it, class it, and transform it.
    nodeElements = node.enter().append('g')
        .classed('node', true)
        .attr('transform', (d) -> "translate(#{d.x}, #{d.y})")

    nodeElements.append('circle')
        .attr('r', 10)
        .on 'click', (d) -> 
            selectOnly = false

            if selectedNode?
                unselect_only = selectedNode is this
                d3.select(selectedNode).classed('selected', false)
                selectedNode = undefined

            if unselect_only
                d3.select('#node-info-container').style('visibility', "hidden")
            else
                d3.select('#node-info-container').style('visibility', null)
                d3.select(this).classed('selected', true)
                selectedNode = this
                explain_node(d)


    nodeElements.append('text')
        .attr('y', (d) -> -20)
        .attr('dy', '.35em')
        .attr('text-anchor', 'middle')
        .text((d) -> if d.children then d.name else '')
        .style('fill-opacity', 1)

    nodeElements.append('text')
        .attr('y', (d) -> 20)
        .attr('dy', '.35em')
        .attr('text-anchor', 'middle')
        .text((d) -> 
            if d.children or d._children
                "< #{d3.round(d.threshold, 3)}" 
            else 
                "[#{d.values.join(' ')}]"
        )
        .style('fill-opacity', 1)

    svg.selectAll('path.link').data([]).exit().remove()

    link = svg.selectAll('path.link')
        .data(links, (d) -> d.target.id)

    link.enter().insert('path', 'g')
        .attr('class', 'link')
        .attr('d', diagonal)

#d3.json 'tree_data.json', (err, treeData) -> 
#    unless err
#        update(treeData[0])

window.updateTreePortrayal = (jsonString) ->
    update(JSON.parse(jsonString))
