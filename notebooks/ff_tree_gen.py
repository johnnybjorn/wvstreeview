# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
import os
import warnings

from ipywidgets import (IntSlider, widgets, Dropdown, Select, HTML,
                        Text, Button, Latex)
from IPython.display import display, clear_output, Javascript

from wvs.io import load_all_waves
from wvs.query import select_countries_names, describe_query, execute_query
from wvs.tree_gen import annotate_univ_tree, infer_tree, extract_roc_auc_score
from wvs.univ_tree import transcribe_classifier
from wvs.util import WVSException, parse_literate_json

warnings.filterwarnings("ignore",category=DeprecationWarning)

###############################################################################
#                       ___ _       _           _
#                      / _ \ | ___ | |__   __ _| |___
#                     / /_\/ |/ _ \| '_ \ / _` | / __|
#                    / /_\\| | (_) | |_) | (_| | \__ \
#                    \____/|_|\___/|_.__/ \__,_|_|___/
#
###############################################################################

WAVE, COUNTRY = 'WV6', None
CODEBOOK, SUB_DF = None, None
MODEL, UNIV_TREE, ROC_AUC = None, None, None

###############################################################################
#                __                 _      ___      _        
#               / /  ___   __ _  __| |    /   \__ _| |_ __ _ 
#              / /  / _ \ / _` |/ _` |   / /\ / _` | __/ _` |
#             / /__| (_) | (_| | (_| |  / /_// (_| | || (_| |
#             \____/\___/ \__,_|\__,_| /___,' \__,_|\__\__,_|
#                                               
###############################################################################

SOURCE_DATA_PATH = os.path.join(os.pardir, 'data', 'clean', 'weakly_imputed')

# Loading the database is a slow operation. When working on UI components,
# there is no need to reload.
WAVES = globals().get('WAVES') or load_all_waves(SOURCE_DATA_PATH)

###############################################################################
#          __    __ _     _            _      _____  ___              
#         / / /\ \ (_) __| | __ _  ___| |_    \_   \/ __\_ _  ___ ___ 
#         \ \/  \/ / |/ _` |/ _` |/ _ \ __|    / /\/ _\/ _` |/ __/ _ \
#          \  /\  /| | (_| | (_| |  __/ |_  /\/ /_/ / | (_| | (_|  __/
#           \/  \/ |_|\__,_|\__, |\___|\__| \____/\/   \__,_|\___\___|
#                  |___/                                     
###############################################################################


def build_tree_notebook():
    univ_json_str = json.dumps(build_trust_tree()).replace("'", "\\'")
    display(Javascript("updateTreePortrayal('{}');".format(univ_json_str)))


def build_tree_click(_):
    try:
        clear_output(False)  # Mostly, clear last error message.
        build_tree_notebook()
        flash_widget.value = '<div class="bg-success">Generated</div>'
    except WVSException, e:
        clear_output()
        flash_widget.value = '<div class="bg-danger">{}</div>'.format(e.msg)

###############################################################################

flash_widget = widgets.HTML(value="Enter Values Here")

###############################################################################

criterion_widget = widgets.Dropdown(options=['gini', 'entropy'],
                                   value='gini', description='Criterion:')

###############################################################################

roc_label_widget = Latex(value="AUC ROC: [NA]")

###############################################################################

tree_depth_widget = IntSlider(2, min=1, max=5, description="Depth:")

###############################################################################

# Remove WV4 wave right now. I need a dichotomizer.
wave_widget = Dropdown(options=sorted(WAVES.keys()),
                       value='WV6', description='Wave')

###############################################################################


def select_countries_in_wave(wave):
    return select_countries_names(WAVES[wave][0], [u'Germany',
                                                   u'India',
                                                   u'United States'])

country_name_widget = Select(options=select_countries_in_wave(WAVE),
                             description="Country:")

###############################################################################

dep_var_widget = Select(options=['--'], description="Dep. Var", width=600)
ind_var_widget = widgets.SelectMultiple(options=['--'], rows=20,
                                        description="Ind. Vars", width=600)

###############################################################################

gen_btn = widgets.Button(description='Generate Tree')

###############################################################################


def wave_changed(trait_name, old_value, new_value):
    selected_country = country_name_widget.value
    new_countries = select_countries_in_wave(wave_widget.value)
    country_name_widget.options = new_countries
    if selected_country in new_countries:
        country_name_widget.selected_label = selected_country


def country_changed(trait_name, old_value, new_value):
    dep_var_widget.options = ["-"]
    ind_var_widget.options = ["-"]
    q = describe_query(wave_widget.value, country_name_widget.value)
    codebook, df = execute_query(q, *WAVES[WAVE])

    selection = []
    for k in sorted(codebook.keys()):
        desc = k + " - " + codebook[k].get('desc', "")[0:400]
        selection.append((desc, k))

    dep_var_widget.options = [(d, k) for d, k in selection
                              if len(df[k].unique()) == 2]
    ind_var_widget.options = selection

###############################################################################

# Insert me into a IPython Notebook Cell
study_portrayal = widgets.Box(children=[
    flash_widget, wave_widget, country_name_widget,
    dep_var_widget,
    Latex("To select more than one, hold ⌘ key while pressing each item."),
    ind_var_widget,
    tree_depth_widget,
    criterion_widget,
    gen_btn
])

###############################################################################

wave_widget.on_trait_change(wave_changed)
country_name_widget.on_trait_change(country_changed)
gen_btn.on_click(build_tree_click)

###############################################################################


def _assign_wave_and_country_to_globals():
    global WAVE, COUNTRY

    WAVE = wave_widget.value
    COUNTRY = country_name_widget.value


def build_trust_tree():
    global CODEBOOK, SUB_DF, MODEL, UNIV_TREE, X_LABELS, ROC_AUC

    _assign_wave_and_country_to_globals()

    dep_var = dep_var_widget.value
    include = set([dep_var] + list(ind_var_widget.value))

    # Generate then execute the query.
    q = describe_query(WAVE, COUNTRY, including=include)
    codebook, df = execute_query(q, *WAVES[WAVE])

    # Verify that the dep variable exists.
    if dep_var not in df.columns:
        raise WVSException("`{}` not found in the data".format(dep_var))

    # Infer the tree at the widget-specified maximum depth.
    clf, x_labels = infer_tree(df, dep_var,
                               tree_depth_widget.value,
                               criterion=criterion_widget.value)

    # Build the universal tree.
    univ = transcribe_classifier(clf)
    annotate_univ_tree(univ, clf.tree_, dep_var, x_labels, codebook)

    # Assign globals for REPL use.
    CODEBOOK, MODEL, SUB_DF, UNIV_TREE = codebook, clf, df, univ

    if len(MODEL.classes_) == 2:
        ROC_AUC = extract_roc_auc_score(MODEL, SUB_DF, x_labels, dep_var)
        roc_label_widget.value = "AUC ROC: {}".format(ROC_AUC)
    else:
        ROC_AUC = -1
        roc_label_widget.value = "AUC ROC: [NA]"

    return univ


###############################################################################



###############################################################################

lookup_variable = Text(description='Variable Name')
lookup_btn = Button(description='Lookup')


def lookup(_):
    global CODEBOOK, SUB_DF
    clear_output(False)  # Mostly, clear last error message.
    v = lookup_variable.value
    coding = CODEBOOK[v]
    description = coding['desc']
    values = SUB_DF[v].value_counts().sort_index()
    values.index = [str(i) + ": " + coding['coding'][i] for i in values.index]
    clear_output(wait=True)
    (values / values.sum()).plot(kind='bar', title=description, rot=45)

lookup_btn.on_click(lookup)

lookup_portrayal = widgets.HBox(children=[lookup_variable, lookup_btn])

###############################################################################

D3TREE_VIEW_DISPLAYED = False


def d3tree_view():
    global D3TREE_VIEW_DISPLAYED
    if not D3TREE_VIEW_DISPLAYED:
        with open("d3viewer/portrayal_snippet.html") as f:
            display(HTML(f.read()))
        with open("d3viewer/setup_snippet.html") as f:
            display(HTML(f.read()))
        D3TREE_VIEW_DISPLAYED = True

###############################################################################
