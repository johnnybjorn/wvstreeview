from __future__ import unicode_literals

import os
from ipywidgets import widgets, Dropdown, HTML, Select
from wvs.io import load_coding, survey_paths
from IPython.display import clear_output


###############################################################################
#                                          _      _
#                          /\/\   ___   __| | ___| |
#                         /    \ / _ \ / _` |/ _ \ |
#                        / /\/\ \ (_) | (_| |  __/ |
#                        \/    \/\___/ \__,_|\___|_|
#
###############################################################################

WAVES = {}
DATA_DIR = os.path.join(os.pardir, 'data', 'clean', 'weakly_imputed')
for wave, path in survey_paths(DATA_DIR):
    json_file = [s for s in os.listdir(path) if s.endswith('.json')][0]
    WAVES[wave] = load_coding(os.path.join(path, json_file))['codebook']

###############################################################################
#                                _
#                         /\   /(_) _____      __
#                         \ \ / / |/ _ \ \ /\ / /
#                          \ V /| |  __/\ V  V /
#                           \_/ |_|\___| \_/\_/
#
###############################################################################

wave_widget = Dropdown(options=sorted(WAVES.keys()),
                       value='WV6', description='Wave')

var_widget = Select(options=sorted(WAVES[wave_widget.value].keys()),
                    value='AUTONOMY',
                    description='Variable')

desc_button = widgets.Button(description='Describe')

output_widget = HTML()

# Insert me into a IPython Notebook Cell
desc_var_portrayal = widgets.Box(children=[
    wave_widget, var_widget, desc_button, output_widget
])

###############################################################################
#                   ___            _             _ _
#                  / __\___  _ __ | |_ _ __ ___ | | | ___ _ __
#                 / /  / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__|
#                / /__| (_) | | | | |_| | | (_) | | |  __/ |
#                \____/\___/|_| |_|\__|_|  \___/|_|_|\___|_|
#
###############################################################################


def wave_changed(trait_name, old_value, new_value):
    var_widget.options = sorted(WAVES[wave_widget.value].keys())


def show_desc(_):
    clear_output(True)
    coding = WAVES[wave_widget.value][var_widget.value]

    rows = ["<h3>{}: {}</h3>".format(var_widget.value, coding['desc']),
            '<table border="1" cellspacing="2" cellpadding="2">',
            "<tr><th>Code</th><th>Description</th></tr>"]

    coding = coding['coding']
    for k in sorted(coding.keys()):
        desc = coding[k]
        rows.append("<td>{}</td><td>{}</t><tr>".format(k, desc))
    rows.append("</table>")

    output_widget.value = "\n".join(rows)

wave_widget.on_trait_change(wave_changed)
desc_button.on_click(show_desc)
