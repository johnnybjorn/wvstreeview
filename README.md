# README

## Installation

### Step 1

The only requirement for this application is Python with various packages. 
Most people in acadamia just use Anaconda. (It's convenient if you have less 
experience building packages and maintaining system libraries. And, it really 
does just save time in general.) 

The command line installer sets up necessary `.bash_profile` `PATH` amendments. 
(Make sure when it asks you to do so, you say 'yes'.) The graphical installer 
probably does the same, but I have no machine to try it out on. 


### Step 2
Clone the repository from [https://bitbucket.org/johnnybjorn/wvstreeview](https://bitbucket.org/johnnybjorn/wvstreeview), then:

```
$ cd wvstreeview
$ bash setup_environment.sh
```
This sets up an Anaconda (i.e Python) `virtual_env` with the exact dependencies 
of this project. No need to manually install things.

### Step 3 

As per Restriction 1.c, ``the data files themselves are not redistributed.'' To avoid the possibility of DCMA violation, the data are not shared in the repository. Unfortunately, Javascript-based click-wrap frustrates efforts at automating data download. So, each researcher [must download the data themselves](http://www.worldvaluessurvey.org/). My scripts use the CSV format. The relevant file are:

- `WV1_Data_ascii_delimited_v_2015_04_18.zip`
- `WV2_Data_ascii_delimited_v_2015_04_18.zip`
- `WV3_Data_ascii_delimited_v_2015_04_18.zip`
- `WV4_Data_ascii_delimited_v_2015_04_18.zip`
- `WV5_Data_ascii_delimited_v_2015_04_18.zip`
- `WV6_Data_ascii_delimited_v_2015_04_18.zip`

Once downloaded, place them in the `data` directory. Then, unzip them there. After unzipping, run,

```
Make
```

### Step 4

Run the notebook generator by running the following from the command line. (I 
assume you're still in the `wvs_tree_viewer` directory.)

```bash run_viewer.sh```

This runs IPython notebook in the correct environment (i.e. `wvs`). It provides
a visual environment. Click `Trust_Tree_Generation.ipynb`.  Then, follow the 
instructions in there for playing with that script.

