.PHONY: build_json_sts_defs
build_json_sts_defs:
	PYTHONPATH=. bin/sts_to_json.py

.PHONY: tests
tests:
	PYTHONPATH=. py.test tests

.PHONY: impute_weakly
impute_weakly:
	PYTHONPATH=. bin/impute_weakly.py